# Input data and R scripts for manuscript "Expansion of US wood pellet industry points to positive trends but the need for continued monitoring"
**REPOSITORY CITATION**

Mirzaee, Ashkan. Data and scripts for manuscript "Expansion of US wood pellet industry points to positive trends but the need for continued monitoring" (2020). https://doi.org/10.6084/m9.figshare.13138802

**RELATED PUBLICATION**

Aguilar, F.X., Mirzaee, A., McGarvey, R.G. et al. Expansion of US wood pellet industry points to positive trends but the need for continued monitoring. Sci Rep 10, 18607 (2020). https://doi.org/10.1038/s41598-020-75403-z

## Access conditions
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.
Sourcecode is available under a [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Contents
All input data and R scripts to provide manuscript results and graphics. This is a backup repository of input data for [rc-biomass](https://gitlab.com/ashki23/rc-biomass).

## Contact information
- Ashkan Mirzaee: amirzaee@mail.missouri.edu

## System requirement
The workflow requires a Unix shell which is a Linux or macOS terminal or a [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) on Windows 10.

## Software
The following software is required:

- [Miniconda3](https://docs.conda.io/projects/conda/en/latest/user-guide/install/#regular-installation)

## Workflow
The workflow includes:

- Clone the repositoty
- Create a conda env for R and required packages
- Run the R script

For running the workflow, open a Unix shell (Linux or macOS terminal or [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) on Windows 10) and run:

```bash
git clone git@gitlab.com:ashki23/backup-biomass.git
cd backup-biomass
conda create --yes --prefix ./bio_r_env --channel conda-forge --file ./bio_r_env.txt
ln -s ${PWD}/bio_r_env/lib/libproj.so ${PWD}/bio_r_env/lib/libproj.so.15
conda activate ./bio_r_env
Rscript panel_regression.R
```
---
<div align="center">
Copyright 2019-2020, [Ashkan Mirzaee](https://ashki23.github.io/index.html) | Content is available under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) | Sourcecode licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
</div>

