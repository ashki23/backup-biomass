#!/usr/bin/env python

import os
import sys
import json

## Open JSON inputs
config = json.load(open(sys.argv[1]))
pellet_data = json.load(open(sys.argv[2]))

## Select states from pellet_date.json to download their SQLite databases
states = []
for s in pellet_data:
    states.extend([s['state']] + s['neighbors'])

## Create job files to download FIA databases
for i in set(states):
    print('************* Download FIA database for:', i, '***************')
    f = open('./fia_sqlite/job-%s.sh' % (i,),'w')
    f.write("""#!/bin/bash

#SBATCH --cpus-per-task=2
#SBATCH --mem=8G

echo $SLURM_JOB_ID >> fia-down-jobid.log
wget -c https://apps.fs.usda.gov/fia/datamart/Databases/SQLite_FIADB_%s.zip -O ${FIA}/SQLite_FIADB_%s.zip
if [ ! -d ${FIA}/FS_FIADB_%s ]; then
unzip -n ${FIA}/SQLite_FIADB_%s.zip -d ${FIA}/FS_FIADB_%s
mv ${FIA}/FS_FIADB_%s/*.db ${FIA}/database/FS_FIADB_%s.db
fi
    """ % (i,i,i,i,i,i,i))
    f.close()

    ## Send the job file to run
    os.system('sbatch ${FIA}/job-%s.sh' % (i,))

## Create jobfiles of SQL queries for each attribute
for j in config['attribute_cd']:
    print('************* Attribute code:', j, '**************')
    f = open('./fia_sqlite/att-sql-%s.py' % (j,),'w')
    f.write("""#!/usr/bin/env python

import sys
import json
import sqlite3
import collections

config = json.load(open(sys.argv[1]))
attribute_sql = json.load(open(sys.argv[2]))
pellet_data = json.load(open(sys.argv[3]))

att_pellet = collections.defaultdict(list)
for l in pellet_data:
    print(l['name'])
    states = [l['state']] + l['neighbors']
    for y in config['year']: 
        m = [None]
        itr = 0
        while m[0] is None:
            m = []
            if itr %s 2 == 0:
                y += itr
            else:
                y -= itr
            print(y)
            for i in states:
                datab = "./fia_sqlite/database/FS_FIADB_%s.db" %s %s
                db = sqlite3.connect(datab)
                db.enable_load_extension(True)
                db.load_extension("mod_spatialite.so")
                c = db.cursor()
                info = (y, l['lon'], l['lat'], l['radius'])
                c.execute(','.join(attribute_sql['%s' %s %s]), (info))
                m.append(c.fetchone()[0])
                c.close()
                db.commit()
                db.close()
                print(m)
                if m[0] == None:
                    break
            itr += 1
            if itr > 10:
                break
        total = sum([x for x in m if x is not None])
        att_pellet[l['unit_id']].append({'%s_%s' %s (%s,%s): total})
        m = []
    
with open('./fia_sqlite/att-pellet-%s.json', 'w') as f:
    json.dump(att_pellet, f)
""" % ('%','%s','%','i','%s','%',j,'%s','%s','%',j,'y',j))
    f.close()

    print('************* Bashfile:', j, '***************')
    r = open('./fia_sqlite/att-sql-%s.sh' % (j,),'w')
    r.write("""#!/bin/bash
    
    echo "$SLURM_JOB_ID" >> fia-sql-jobid.log
    srun python ${FIA}/att-sql-%s.py config.json attributes-sql.json pellet_data.json
    """ % (j,))  
    r.close()

## Send the job files to run
os.system('sleep 3')
for j in config['attribute_cd']:
    os.system("""
    JOBID=$(cat fia-down-jobid.log | tr '\n' ',' | grep -Po '.*(?=,)')
    sbatch --dependency=afterok:$(echo ${JOBID}) ${FIA}/att-sql-%s.sh
    """ % (j,))

print('************* Obtain level of attributes ***************')

## Create a job file to add level of the attribute to the main JSON output
s = open('./fia_sqlite/att-fia-sql.py','w')
s.write("""#!/usr/bin/env python

import sys
import json
import collections

config = json.load(open(sys.argv[1]))
pellet_data = json.load(open(sys.argv[2]))

## Create a dictionary of all FIA attribute levels for each Pellet ID
att_pellet_all = collections.defaultdict(list)
for j in config['attribute_cd']:
    with open('./fia_sqlite/att-pellet-%s.json' % j, 'r') as js:
        js_data = json.load(js)
    for l in js_data:
        levels = js_data[l]
        for n in levels:
            att_year = list(n.keys())
            att_pellet_all[l].append({att_year[0]:n[att_year[0]]})

# print(att_pellet_all)

## Add the attribite levels to pellet_data.json file
for p in pellet_data:
    levels = att_pellet_all[p['unit_id']]
    for n in levels:
        att_year = list(n.keys())
        p[att_year[0]] = n[att_year[0]]

## JSON output
with open('pellet_data.json', 'w') as f:
    json.dump(pellet_data, f)
""")
s.close()

## Send the job file to run                                                                                                                                           
os.system("""
sleep 3
JOBID=$(cat fia-sql-jobid.log | tr '\n' ',' | grep -Po '.*(?=,)')
srun --dependency=afterany:$(echo ${JOBID}) python ${FIA}/att-fia-sql.py config.json pellet_data.json
rm fia-*.log
""")
